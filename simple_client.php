<?php

require 'vendor/autoload.php';

function simple_client($url, $wstoken, $wsfunction, array $fields) {
    $client = new GuzzleHttp\Client();
    $request = $client->createRequest('POST', $url, array(
        'query' => array(
            'wstoken' => $wstoken,
            'wsfunction' => $wsfunction,
            'moodlewsrestformat' => 'json'
        )
    ));
    $postBody = $request->getBody();
    foreach ($fields as $key => $value) {
        $postBody->setField($key, $value);
    }
    $response = $client->send($request);
    return $response;
}

