<?php

require 'config.php';
require 'simple_client.php';

$fields = array();
$fields['courseid'] = 2; // course id de exemplo
$fields['classid'] = 1; // class id de exemplo

$response = simple_client(
    $url,
    $wstoken,
    'local_aval_get_class_attendance',
    $fields
);
header('Content-Type: text/plain; charset=UTF-8');
var_export($response->json());
