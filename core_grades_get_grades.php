<?php

require 'config.php';
require 'simple_client.php';

$fields = array();
$fields['courseid'] = 2; // course id de exemplo
$fields['userids'] = array(2); // user ids de exemplo

$response = simple_client($url, $wstoken, 'core_grades_get_grades', $fields);
header('Content-Type: text/plain; charset=UTF-8');
var_export($response->json());
