<?php

require 'config.php';
require 'simple_client.php';

$users = array();
if (($handle = fopen('usersdata.csv', 'r')) !== false) {
    while (($data = fgetcsv($handle, 0, ';')) !== false) {
        $user = array();
        $user['username'] = $data[0];
        $user['password'] = $data[1];
        $user['firstname'] = $data[2];
        $user['lastname'] = $data[3];
        $user['email'] = $data[4];
        $user['city'] = $data[5];
        $user['idnumber'] = $data[6];
        array_push($users, $user);
    }
    fclose($handle);
}
array_shift($users);

$response = simple_client(
    $url,
    $wstoken,
    'core_user_create_users',
    compact('users')
);
header('Content-Type: text/plain; charset=UTF-8');
var_export($response->json());
