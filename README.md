# Moodle Web Services

Este guia descreve os passos necessários para configurar e usar web services no Moodle. É baseado no guia ["Using Moodle Web Services"](http://www.rumours.co.nz/manuals/using_moodle_web_services.htm). Os passos foram testados na versão 2.9 do Moodle.

## Habilitar web services

* Vá para **Painel - Administração do site - Opções avançadas**. Marque a opção **Habilitar serviços web (web services)**. Clique no botão **Salvar mudanças** no final da página.
* Vá para **Painel - Administração do site - Plugins - Web services - Gerenciar protocolos**. Habilite o protocolo REST (clicando no marcador na coluna **Habilitar**). Marque a opção **Documentação de serviços web**. Clique no botão **Salvar mudanças** no final da página.

## Criar um web service

* Vá para **Painel - Administração do site - Plugins - Web services - Serviços externos**. Clique em **Adicionar**. Digite um nome no campo **Nome** ("webservice", por exemplo) e marque a opção **Ativado**. Clique no botão **Adicionar serviço** no final da página.
* Adicione funções ao web service criado. Vá para **Painel - Administração do site - Plugins - Web services - Serviços externos - Funções** e adicione as seguintes funções:
    - **core_course_create_courses** - para criar cursos.
    - **core_grades_get_grades** - para obter as notas.
    - **core_user_create_users** - para criar usuários.
    - **core_user_get_users** - para obter informações de usuários.
    - **enrol_manual_enrol_users** - para inscrever usuários em cursos.

![Funções](service_functions.png)

Na imagem acima, observe a localização da página na árvore **Administração do site**, veja as 5 funções adicionadas ao serviço e preste atenção na coluna **Capacidades exigidas** - essas são as permissões de acesso exigidas para executar as funções do web service.

## Criar uma conta de usuário para usar web services

* Vá para **Painel - Administração do site - Usuários - Contas - Adicionar novo usuário**. Digite um nome de usuário no campo **Identificação de usuário** ("webservice", por exemplo) e mantenha os outros campos com os valores padrões.

## Criar um novo papel global

* Vá para **Painel - Administração do site - Usuários - Permissões - Definir papéis**. Clique no botão **Acrescentar um novo papel** e, depois, clique no botão **Continuar**.
* Digite um nome no campo **Nome breve** e **Nome completo personalizado** ("webservice", por exemplo). Marque a opção **Sistema** no campo **Tipos de contexto onde esse papel pode ser atribuído**. Selecione as opções **Professor** e **Estudante** no campo **Permitir atribuição de papel** (isso é necessário para que seja possível inscrever usuários em cursos como professor e como aluno). Clique no botão **Criar este papel** no final da página.

## Atribuir capacidades ao papel

Agora, é necessário atribuir capacidades ao papel criado. É importante que esse papel tenha todas as capacidades exigidas para executar as funções do web service. Quando uma capacidade está ausente, o Moodle trata as exceções internamente, dificultando a correção de erros. As capacidades exigidas são as seguintes:

* Capacidades exigidas informadas pelo Moodle:
    - **webservice/rest:use** - ou **Use o protocolo REST**.
    - **moodle/course:create** - ou **Criar cursos**.
    - **moodle/course:visibility** - ou **Ocultar/Mostrar cursos**.
    - **moodle/grade:view** - ou **Ver suas avaliações**.
    - **moodle/grade:viewall** - ou **Ver avaliações de outros usuários**.
    - **moodle/grade:viewhidden** - ou **Ver avaliações ocultas do usuário**.
    - **moodle/user:create** - ou **Criação de usuários**.
    - **moodle/course:useremail** - ou **Habilitar/desabilitar endereço de e-mail**.
    - **moodle/user:update** - ou **Atualizar perfil de usuário**.
    - **moodle/user:viewdetails** - ou **Visualizar perfil de usuário**.
    - **moodle/user:viewhiddendetails** - ou **Visualizar detalhes ocultos dos usuários**.
    - **enrol/manual:enrol** - ou **Inscrever usuários**.
* Capacidades exigidas descobertas pela leitura do código fonte do Moodle e por tentativa e erro:
    - **moodle/course:view** - ou **Visualizar cursos sem participar**.
    - **moodle/course:viewhiddencourses** - ou **Visualizar cursos que estão ocultos**.
    - **moodle/role:assign** - ou **Atribuir papéis a usuários**.
    - **moodle/user:viewalldetails** - ou **Ver informação completa do usuário**.

## Atribuir usuário ao papel

Atribua o usuário criado no terceiro passo ao papel criado no passo anterior. Vá para **Painel - Administração do site - Usuários - Permissões - Atribuir papéis globais**. Clique no papel criado no passo anterior e, na página seguinte, adicione o usuário criado no terceiro passo. Agora, o usuário tem todas as capacidades exigidas para executar as funções do web service.

## Criar um token

O token é uma das formas de autenticação de usuários disponíveis no Moodle. Vá para **Painel - Administração do site - Plugins - Web services - Gerenciar tokens**. Clique em **Adicionar**. Selecione o usuário no campo **Usuário**, selecione o web service no campo **Serviço** e clique no botão **Salvar mudanças** no final da página.

## Funcionamento dos exemplos

A função [`simple_client`](simple_client.php) cria um cliente, cria e envia uma requisição e retorna a resposta do servidor. O cliente usa a biblioteca [Guzzle](http://docs.guzzlephp.org/) e cria a requisição conforme a documentação dos web services, que está em **Painel - Administração do site - Plugins - Web services - Documentação da API**. A resposta é passada como parâmetro para a função [`var_export`](https://php.net/var_export).

    Psr\Http\Message\ResponseInterface simple_client ( string $url, string $wstoken, string $wsfunction, array $fields )

As variáveis `$url` e `$wstoken` em **config.php** devem ser preenchidas com a URL dos web services REST do Moodle e com o token criado no sétimo passo.

## Exemplos de código

* [**Criando usuários** - core_user_create_users.php](core_user_create_users.php).
* [**Criando cursos** - core_course_create_courses.php](core_course_create_courses.php).
* [**Inscrevendo usuários em cursos** - enrol_manual_enrol_users.php](enrol_manual_enrol_users.php).
* [**Obtendo as notas** - core_grades_get_grades.php](core_grades_get_grades.php).

## Usando os exemplos de código

Antes de usar os exemplos de código, é necessário instalar o [Composer](https://getcomposer.org/) e baixar as dependências do projeto executando o comando `composer install`. Depois, é necessário criar um servidor executando o comando `php -S localhost:8080 -t .`, por exemplo, que é acessível pelo endereço http://localhost:8080/.

## Mais informações

Os links abaixo contém páginas da documentação do Moodle com mais informações sobre como criar clientes de web services no Moodle e sobre as funções de web services disponíveis no Moodle.

* [Creating a web service client](https://docs.moodle.org/dev/Web_services)
* [Web service API functions](https://docs.moodle.org/dev/Web_service_API_functions)

## Funções de web service da extensão local_aval

A extensão **local_aval** adiciona o web service **avalservice** e as funções de web services **local_aval_get_class_attendance** e **local_aval_get_course_classes**. Para usá-las, é necessário que o usuário tenha um papel com as capacidades exigidas e um token para um web service com essas funções. Essas funções foram criadas conforme a página [Adding a web service to a plugin](https://docs.moodle.org/dev/Adding_a_web_service_to_a_plugin) da documentação do Moodle.

### local_aval_get_class_attendance

A função **local_aval_get_class_attendance** invoca o método `get_class_attendance` da classe `local_aval_external` em **externallib.php**, que fica disponível após a instalação da extensão **local_aval**. Os parâmetros `courseid` e `userid` são obrigatórios. A tabela que armazena os dados das chamadas é chamada de **local_aval_attendance** e sua definição na linguagem XMLDB do Moodle está disponível em **db/install.xml**.

#### Argumentos REST (parâmetros POST)

    courseid= int
    userid= int

#### Resposta

    list of (
        object {
            id int // ID da presença
            classid int // ID da aula
            userid int // ID do usuário (aluno) no Moodle
            status int // Status da presença
            timemodified int // Data e hora da última modificação
            modifiedby int // ID do usuário (professor) da última modificão
        }
    )

#### Exemplo de resposta (Estrutura PHP)

    Array (
        [0] => Array (
            [id] => int
            [classid] => int
            [userid] => int
            [status] => int
            [timemodified] => int
            [modifiedby] => int
        )
    )

### local_aval_get_course_classes

A função **local_aval_get_course_classes** invoca o método `get_course_classes` da classe `local_aval_external` em **externallib.php**, que fica disponível após a instalação da extensão **local_aval**. Os parâmetros `courseid` e `idnumber` são opcionais, mas pelo menos um tem que ser definido. A tabela que armazena os dados das aulas é chamada de **local_aval_class** e sua definição na linguagem XMLDB do Moodle está disponível em **db/install.xml**.

#### Argumentos REST (parâmetros POST)

    courseid= int
    idnumber= string

#### Resposta

    list of (
        object {
            id int // ID da aula
            courseid int // ID do curso no Moodle
            timestart int // Data e hora de início
            timeend int // Data e hora de término
            description string // Descrição
            timemodified int // Data e hora da última modificação
            modifiedby int // ID do usuário (professor) da última modificão
        }
    )

#### Exemplo de resposta (Estrutura PHP)

    Array (
        [0] => Array (
            [id] => int
            [courseid] => int
            [timestart] => int
            [timeend] => int
            [description] => string
            [timemodified] => int
            [modifiedby] => int
        )
    )

### Exemplos de código

* [**Obtendo as aulas de um curso** - local_aval_get_course_classes.php](local_aval_get_course_classes.php).
* [**Obtendo a chamada de uma aula** - local_aval_get_class_attendance.php](local_aval_get_class_attendance.php).
