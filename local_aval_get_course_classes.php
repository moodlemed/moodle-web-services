<?php

require 'config.php';
require 'simple_client.php';

$fields = array();
$fields['courseid'] = 2; // course id de exemplo
// $fields['idnumber'] = ''; // id number de exemplo

$response = simple_client(
    $url,
    $wstoken,
    'local_aval_get_course_classes',
    $fields
);
header('Content-Type: text/plain; charset=UTF-8');
var_export($response->json());
