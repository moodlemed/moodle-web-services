<?php

require 'config.php';
require 'simple_client.php';

$courses = array();
if (($handle = fopen('coursesdata.csv', 'r')) !== false) {
    while (($data = fgetcsv($handle, 0, ';')) !== false) {
        $course = array();
        $course['shortname'] = $data[0];
        $course['fullname'] = $data[1];
        $course['idnumber'] = $data[2];
        $course['categoryid'] = $data[3];
        array_push($courses, $course);
    }
    fclose($handle);
}
array_shift($courses);

$response = simple_client(
    $url,
    $wstoken,
    'core_course_create_courses',
    compact('courses'));
header('Content-Type: text/plain; charset=UTF-8');
var_export($response->json());
