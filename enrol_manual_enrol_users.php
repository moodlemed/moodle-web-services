<?php

require 'config.php';
require 'simple_client.php';

$criteria = array();
if (($handle = fopen('usersdata.csv', 'r')) !== false) {
    while (($data = fgetcsv($handle, 0, ';')) !== false) {
        $criterion = array();
        $criterion['key'] = 'idnumber';
        $criterion['value'] = $data[6];
        array_push($criteria, $criterion);
    }
    fclose($handle);
}
array_shift($criteria);
$userids = array();
foreach ($criteria as $criterion) {
    $fields = array('criteria' => array($criterion));
    // isto é lento e o ideal é salvar o user id do moodle
    $response = simple_client($url, $wstoken, 'core_user_get_users', $fields);
    array_push($userids, $response->json()['users'][0]['id']);
}

$courseid = 2; // course id de exemplo
$enrolments = array();
$enrolment = array();
$enrolment['roleid'] = 3; // role id dos professores
$enrolment['userid'] = $userids[0];
$enrolment['courseid'] = $courseid;
array_push($enrolments, $enrolment);
array_shift($userids);
foreach ($userids as $userid) {
    $enrolment = array();
    $enrolment['roleid'] = 5; // role id dos alunos
    $enrolment['userid'] = $userid;
    $enrolment['courseid'] = $courseid;
    array_push($enrolments, $enrolment);
}

$response = simple_client(
    $url,
    $wstoken,
    'enrol_manual_enrol_users',
    compact('enrolments')
);
header('Content-Type: text/plain; charset=UTF-8');
var_export($response->json()); // "NULL" significa "sucesso"
